<?php
/**
 * Index page file.
 *
 * PHP version 7
 *
 * @category Example
 * @package  PHPProjectTemplate
 * @author   Robb <robb@robbinharris.com>
 * @license  http://example.com/license.txt Example
 * @link     none
 */

/**
 * Say Hello buddy
 *
 * @param String $name your name
 *
 * @return void
 */
function sayHello($name) : void
{
    echo "Hello $name!";
}
?>

<html>
    <head>
        <title>Visual Studio Code Remote :: PHP</title>
    </head>
    <body>
        <?php

        sayHello('superFly world');

        phpinfo();

        ?>
    </body>
</html>
