#!/bin/bash
set -e

# here we install our dependencies so after rebuild we are always current
composer install

# here we run our coverage, this way you can view test coverage right out of the box
# can be removed if it starts to take to long
vendor/bin/phpunit --coverage-clover cov.xml
