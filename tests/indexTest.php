<?php
/**
 * Test file for index page.
 *
 * PHP version 7
 *
 * @category Example
 * @package  PHPProjectTemplate
 * @author   Robb <robb@robbinharris.com>
 * @license  http://example.com/license.txt Example
 * @link     none
 */

use PHPUnit\Framework\TestCase;

/**
 * The IndexText class contains all the index test page tests
 *
 * @category Example
 * @package  PHPProjectTemplate
 * @author   Robb <robb@robbinharris.com>
 * @license  http://example.com/license.txt Example
 * @link     none
 */
final class IndexTest extends TestCase
{

    /**
     * Test that index page runs without issue
     *
     * @return void
     */
    public function testRenders() : void
    {
        $this->assertEquals(true, true);
    }

    /**
     * Tests stuff
     *
     * @return void
     */
    public function testBleh() : void
    {
        $this->assertEquals(false, false);
    }
}
